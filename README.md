# README #

This is a small Ruby on Rails project created during the Coursera course on Web Application Architecture I, as a part of the course assignments. Nothing special about this.

### What is this repository for? ###

* a minimalistic blog app for practising Ruby on Rails
* Version 0.1

### How do I get set up? ###

* Summary of set up
    - git clone this repository
    - run bundle install from the root directory of this repository
    - rails server should get the application up and running
* Configuration
* Dependencies
    - ruby 2.0.0+
    - rails 4.1.4+
* Database configuration
    - development version runs on SQLite
* How to run tests
* Deployment instructions

### Who do I talk to? ###

**owner** parthan AT technofreak DOT in